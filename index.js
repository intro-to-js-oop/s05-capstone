class Customer {
	constructor(email){
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut() {
		if(this.cart.contents.length === 0){
			return "No Content!!"
		}
		else {
			
			this.cart.contents.forEach(cart => this.orders.push({product: cart, totalAmount: this.cart.computeTotal().totalAmount}));
			

			return this;
		}
	}
}


class Cart {
	constructor(){
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(product, qty){
		this.contents.push({ product: product, quantity: qty});
		return this;
	}

	showCartContents() {
		console.log(this.contents);
		return this;
	}

	updateProductQuantity(product, quantity) {
		this.contents.find(({product}) => product === product).quantity = quantity;
		return this;
	}

	clearCartContents() {
		this.contents = [];
		return this;
	}

	computeTotal() {
		//initialize totalAmount to 0 so that the last totalAmount won't retain and add once the method has been invoke for the second time
		this.totalAmount = 0;
		this.contents.forEach(content => this.totalAmount += content.product.price * content.quantity);
		return this;
	}
}

class Product {
	constructor(name,price){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive() {
		this.isActive = false;
		return this;
	}

	updatePrice(newPrice) {
		this.price = newPrice;
		return this;
	}
}

const john = new Customer('john@mail.com');

console.log(john);

const prodA = new Product('soap', 9.99);
const prodB = new Product('NewSoap', 9.99);


console.log(prodA);

prodA.updatePrice(12.99);
//console.log(prodA);

//prodA.archive();
//console.log(prodA);

//john.cart.addToCart(prodA, 3);
//john.cart.addToCart(prodB, 3);
console.log(john);


//john.cart.updateProductQuantity('soap', 5);
